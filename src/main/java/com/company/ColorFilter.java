package com.company;

import java.awt.*;
import java.util.HashMap;

public class ColorFilter extends AbstractFilter{
    private ImageStore store;
    private int startLine;
    private int endLine;
    private HashMap <String, Integer> filterParams;

    public ColorFilter(ImageStore store, int startLine, int endLine, HashMap <String, Integer> filterParams) {
        this.store = store;
        this.startLine = startLine;
        this.endLine = endLine;
        this.filterParams = filterParams;
    }

    @Override
    public void runFilter() {

        for(int y = startLine; y < endLine; y++){
            for(int x = 0; x < store.image.getWidth(); x++){
                Color color = new Color(store.image.getRGB(x, y));
                int L = (int) (0.2126*color.getRed() + 0.7152*color.getGreen() + 0.0722*color.getBlue());
                int redColorFilter = filterParams.get("redColor") * L/255;
                int blueColorFilter = filterParams.get("blueColor") * L/255;
                int greenColorFilter = filterParams.get("greenColor") * L/255;

                Color newColor = new Color(redColorFilter, greenColorFilter, blueColorFilter);
                store.image.setRGB(x, y, newColor.getRGB());
            }
        }
    }
}
