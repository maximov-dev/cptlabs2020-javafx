package com.company;

import java.util.HashMap;

public class Worker extends  Thread {
    private ImageStore store;
    private int threadCount;
    private int startLine;
    private int endLine;
    private HashMap filterParams;
    private String filterType;
    private ImageFilter filter;

    public Worker(ImageStore store, int startLine, int endLine, int threadCount, String filterType, HashMap filterParams)
    {
        this.store = store;
        this.filterType = filterType;
        this.startLine = startLine;
        this.endLine = endLine;
        this.threadCount = threadCount;
        this.filterParams = filterParams;
        this.filter = new ImageFilter(this.store, this.startLine, this.endLine, this.threadCount, this.filterType, this.filterParams);

        store.phaser.register();
    }

    @Override
    public void run(){
        store.phaser.arriveAndAwaitAdvance();

        this.filter.configure().runFilter();

        store.phaser.arriveAndAwaitAdvance();
    }
}
