package com.company;

import java.awt.*;
import java.util.HashMap;


class GaussFilter extends AbstractFilter {
    private ImageStore store;
    private int threadCount;
    private int startLine;
    private int endLine;
    private HashMap <String, Integer> filterParams;
    private final int blurRadius;
    private double[][] weightArr;

    public GaussFilter(ImageStore store, int threadCount, int startLine, int endLine, HashMap <String, Integer> filterParams){
        this.store = store;
        this.threadCount = threadCount;
        this.startLine = startLine;
        this.endLine = endLine;
        this.filterParams = filterParams;
        this.blurRadius = this.filterParams.get("blurRadius");

        weightArr = new double[blurRadius*2+1][blurRadius*2+1];

        prepareWeightMatrix();
    }

    public void runFilter() {
        int imgCorrectedWidth = store.image.getWidth()  - blurRadius*2;
        int imgCorrectedHeight = endLine  - blurRadius * 2;

        for (int x = 0; x < imgCorrectedWidth; x++) {
            for (int y = startLine; y < imgCorrectedHeight; y++) {
                int blurRadiusX = blurRadius+x;
                int blurRadiusY = blurRadius + y;

                int r = (int)getBlurColor(blurRadiusX,blurRadiusY,1);
                int g = (int)getBlurColor(blurRadiusX,blurRadiusY,2);
                int b = (int)getBlurColor(blurRadiusX,blurRadiusY,3);

                Color color = new Color(r,g,b);
                store.image.setRGB(x, y, color.getRGB());
            }
        }

    }

    private double getR(int x,int y){
        int rgb = store.image.getRGB(x, y);

        return (rgb & 0xff0000) >> 16;
    }

    private double getG(int x,int y){
        int rgb = store.image.getRGB(x, y);

        return (rgb & 0xff00) >> 8;
    }

    private double getB(int x,int y){
        int rgb = store.image.getRGB(x, y);

        return (rgb & 0xff);
    }

    private double[][] getColorMatrix(int x, int y, int whichColor){

        int startX = x-blurRadius;
        int startY = y-blurRadius;
        int counter = 0;

        int length = blurRadius*2+1;
        double[][] arr = new double[length][length];

        for (int i=startX ; i<startX+length ;i++){
            for (int j = startY; j < startY+length; j++){
                if (whichColor == 1){
                    arr[counter%length][counter/length] = getR(i,j);
                }else if (whichColor == 2){
                    arr[counter%length][counter/length] = getG(i,j);
                }else if (whichColor == 3){
                    arr[counter%length][counter/length] = getB(i,j);
                }
                counter++;
            }
        }

        return arr;
    }

    private void calculateWeightMatrix(){
        for (int i=0;i<blurRadius*2+1;i++){
            for (int j=0;j<blurRadius*2+1;j++){

                weightArr[i][j] = getWeight(j-blurRadius,blurRadius-i);
            }
        }

    }

    private double getWeight(int x,int y){
        double sigma = (blurRadius*2+1)/2;

        return (1/(2*Math.PI*sigma*sigma))*Math.pow(Math.E,((-(x*x+y*y))/((2*sigma)*(2*sigma))));
    }

    private void getFinalWeightMatrix(){

        int length = blurRadius*2+1;
        double weightSum = 0;

        for (int i = 0;i<length;i++){
            for (int j=0; j<length; j++ ){
                weightSum+=weightArr[i][j];
            }
        }


        for (int i = 0;i<length;i++){
            for (int j=0; j<length; j++ ){
                weightArr[i][j] = weightArr[i][j]/weightSum;
            }
        }

    }

    private double getBlurColor(int x, int y,int whichColor){

        double blurGray = 0;
        double[][] colorMat = getColorMatrix(x,y,whichColor);

        int length = blurRadius*2+1;
        for (int i = 0;i<length;i++){
            for (int j=0; j<length; j++ ){
                blurGray += weightArr[i][j]*colorMat[i][j];
            }
        }

        return blurGray;
    }

    private void prepareWeightMatrix() {
        calculateWeightMatrix();
        getFinalWeightMatrix();
    }

}