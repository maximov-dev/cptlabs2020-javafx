package com.company;

import java.util.HashMap;

public class ImageFilter {
    private ImageStore store;
    private int startLine;
    private int endLine;
    private int threadCount;
    private String filterType;
    private HashMap filterParams;
    private AbstractFilter filter;

    public ImageFilter(ImageStore store, int startLine, int endLine, int threadCount, String filterType, HashMap filterParams) {
        this.store = store;
        this.threadCount = threadCount;
        this.startLine = startLine;
        this.endLine = endLine;
        this.filterType = filterType;
        this.filterParams = filterParams;

    }

    public AbstractFilter configure() {
        switch (this.filterType) {
            case "gauss-filter":
                filter = new GaussFilter(this.store, this.threadCount, this.startLine, this.endLine, filterParams);

                return filter;
            case "color-filter":
                filter = new ColorFilter(this.store, this.startLine, this.endLine, filterParams);

                return filter;
        }

        return null;
    }
}
