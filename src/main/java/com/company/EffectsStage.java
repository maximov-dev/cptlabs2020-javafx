package com.company;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class EffectsStage {
    final Stage effectsStage = new Stage();

    public void openEffectsWindow() {
        final Label closeWindowLabel = new Label("Choose Filter");
        closeWindowLabel.setWrapText(true);
        closeWindowLabel.setStyle("-fx-font-weight: 700");
        closeWindowLabel.setPadding(new Insets(50, 0, 50, 0));


        final Label sliderLabel = new Label("Choose Blur Radius");
        final Slider slider = new Slider(0.0, 16.0, 5.0);
        slider.setShowTickMarks(true);
        slider.setShowTickLabels(true);
        slider.setBlockIncrement(2.0);
        slider.setMajorTickUnit(5.0);
        slider.setMinorTickCount(4);
        slider.setSnapToTicks(true);

        final ColorPicker colorPicker = new ColorPicker();
        colorPicker.setValue(Color.RED);
        final Circle circle = new Circle(50);
        circle.setFill(colorPicker.getValue());
        initColor();
        colorPicker.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                circle.setFill(colorPicker.getValue());
                Color selectedColor = colorPicker.getValue();

                int r = ((int) (selectedColor.getRed() * 255));
                int g = ((int) (selectedColor.getGreen() * 255));
                int b = ((int) (selectedColor.getBlue() * 255));

                System.out.println(selectedColor);
                System.out.println(r);
                System.out.println(g);
                System.out.println(b);

                Main.filterParams.put("redColor", r);
                Main.filterParams.put("greenColor", g);
                Main.filterParams.put("blueColor", b);
            }
        });

        Button closeWindowButton = new Button("OK");
        closeWindowButton.setPrefWidth(80);
        closeWindowButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                effectsStage.close();
            }
        });

        RadioButton colorFilterButton = new RadioButton("Color Filter");
        RadioButton gaussBlurFilterButton = new RadioButton("Gauss Blur Filter");

        ToggleGroup radioGroup = new ToggleGroup();

        colorFilterButton.setToggleGroup(radioGroup);
        gaussBlurFilterButton.setToggleGroup(radioGroup);

        VBox chooseEffectsVBox = new VBox(closeWindowLabel, colorFilterButton, gaussBlurFilterButton, closeWindowButton);

        VBox effectsParamsVBox = new VBox(getEmptyLabel());
        effectsParamsVBox.setAlignment(Pos.CENTER);

        HBox flexHBox = new HBox(chooseEffectsVBox, effectsParamsVBox);

        flexHBox.setAlignment(Pos.CENTER);
        flexHBox.setPadding(new Insets(0, 0, 50, 0));
        Scene scene = new Scene(flexHBox);

        slider.valueProperty().addListener(new ChangeListener<Number>(){

            public void changed(ObservableValue<? extends Number> changed, Number oldValue, Number newValue){
                Main.filterParams.put("blurRadius", newValue.intValue());
            }
        });

        radioGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> ov, Toggle t, Toggle t1) {
                effectsParamsVBox.getChildren().clear();
                RadioButton chk = (RadioButton)t1.getToggleGroup().getSelectedToggle();

                switch (chk.getText()) {
                    case "Color Filter":
                        Main.filterType = "color-filter";

                        FlowPane colorFilterRoot = new FlowPane(Orientation.VERTICAL, 10, 10,circle, colorPicker);

                        effectsParamsVBox.getChildren().add(colorFilterRoot);

                        break;
                    case "Gauss Blur Filter":
                        Main.filterType = "gauss-filter";

                        FlowPane gaussFilterRoot = new FlowPane(Orientation.VERTICAL, 10, 10, slider, sliderLabel);
                        effectsParamsVBox.getChildren().add(gaussFilterRoot);

                        break;
                    default:
                        Main.filterType = "";
                        break;
                }

            }
        });

        effectsStage.setScene(scene);

        effectsStage.setTitle("Choose Filter");
        effectsStage.setWidth(600);
        effectsStage.setHeight(600);

        effectsStage.show();
    }

    private Label getEmptyLabel() {
        return new Label("Empty");
    }

    private void initColor() {
        Main.filterParams.put("redColor", 255);
        Main.filterParams.put("greenColor", 0);
        Main.filterParams.put("blueColor", 0);
    }
}
