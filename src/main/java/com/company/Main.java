package com.company;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.util.HashMap;

public class Main extends Application {
    public static String filterType = "";
    public final int MAX_THREAD_COUNT = Runtime.getRuntime().availableProcessors();
    public static HashMap<String, Integer> filterParams = new HashMap<>();

    private String filePath = "";

    public void start(Stage stage) throws Exception {

        NumberAxis x = new NumberAxis();
        x.setLabel("Threads Count");
        x.setLowerBound(1);

        NumberAxis y = new NumberAxis();
        y.setLabel("Time (ms)");

        final LineChart<Number, Number> lineChart = new LineChart<Number, Number>(x, y);
        final TextField threadCountInputBox = new TextField("");

        Button applyFilterButton = new Button("Apply Filter");
        Button selectFilterButton = new Button("Select Filter");

        final FileChooser fileChooser = new FileChooser();
        Button fileChooserButton = new Button("Select One File and Open");

        final Stage newWindow = new Stage();

        fileChooserButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                File file = fileChooser.showOpenDialog(newWindow);
                if (file != null) {
                    filePath = file.getAbsolutePath();
                }
            }
        });

        selectFilterButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if (filePath.equals("")) {
                    showError("File path doesn't exist");
                    return;
                }

                openEffectsWindow();
            }});

        applyFilterButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                final int threadCount = !threadCountInputBox.getText().isEmpty() ? Integer.parseInt(threadCountInputBox.getText()) : -1;

                if (filePath.equals("")) {
                    showError("File path doesn't exist");
                    return;
                } else if (threadCountInputBox.getText().equals("")) {
                    showError("Please set threads count");
                    return;
                } else if (threadCount > MAX_THREAD_COUNT || threadCount <= 0) {
                    showError("Incorrect threads count...");
                    return;
                } else if (filterType.equals("")) {
                    openEffectsWindow();
                    showError("Incorrect threads count...");
                    return;
                }


                XYChart.Series series = new XYChart.Series();
                series.setName(threadCount + " " + filterType);

                ImageStore store = new ImageStore(filePath);
                store.phaser.register();

                int sampleRate = store.image.getHeight() / threadCount;
                int currentSampleRate = 0;

                for (int i = 1; i <= threadCount; i++) {
                    Worker worker = new Worker(
                            store,
                            currentSampleRate,
                            (currentSampleRate + sampleRate),
                            threadCount,
                            filterType,
                            filterParams
                    );
                    worker.setPriority(Thread.NORM_PRIORITY);
                    worker.start();
                    currentSampleRate += sampleRate;

                }
                long startTime = System.currentTimeMillis();

                store.phaser.arriveAndAwaitAdvance();
                store.phaser.arriveAndAwaitAdvance();
                store.saveResult();

                long endTime = System.currentTimeMillis();

                XYChart.Data d = new XYChart.Data(threadCount, endTime - startTime);
                series.getData().add(d);


                lineChart.getData().add(series);
            }
        });


        Label fileChooserLabel = new Label("1 Choose File");
        fileChooserLabel.setPadding(new Insets(0,15,0,15));
        HBox fileChooserControl = new HBox(fileChooserLabel, fileChooserButton);

        Label selectFilterButtonLabel = new Label("2 Select Filter");
        selectFilterButtonLabel.setPadding(new Insets(0,15,0,15));
        HBox selectFilterControl = new HBox(selectFilterButtonLabel, selectFilterButton);

        Label applyFilterButtonButtonLabel = new Label("3 Apply The Filter");
        applyFilterButtonButtonLabel.setPadding(new Insets(0,15,0,15));
        HBox applyFilterButtonControl = new HBox(applyFilterButtonButtonLabel, applyFilterButton);

        VBox buttons = new VBox(fileChooserControl, selectFilterControl, applyFilterButtonControl);
        buttons.setPadding(new Insets(50,50,50,50));

        Label threadCountInputBoxLabel = new Label("Please enter the number of threads");
        VBox threadCountInputBoxControl = new VBox(threadCountInputBoxLabel, threadCountInputBox);
        threadCountInputBoxControl.setPadding(new Insets(50,50,50,50));

        HBox control = new HBox(threadCountInputBoxControl, buttons);
        control.setAlignment(Pos.CENTER);
        VBox group = new VBox(lineChart, control);
        Scene scene = new Scene(group);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setTitle("Multi Thread Image Processing App");
        stage.setHeight(400);
        stage.setWidth(800);
        stage.show();
    }

    private static void showError(String errorMessage) {
        ErrorStage errorStage = new ErrorStage();
        errorStage.showErrorMessage(errorMessage);
    }

    private static void openEffectsWindow() {
        EffectsStage effectsStage = new EffectsStage();
        effectsStage.openEffectsWindow();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
