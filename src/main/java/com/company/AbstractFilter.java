package com.company;

public abstract class AbstractFilter {
    public abstract void runFilter();
}